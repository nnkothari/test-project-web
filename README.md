# Test Project WEB

### TECH STACK:

* Ract js
* Redux for state management
* Redux-saga for async middleware
* Bootstrap
* Font Awesome

## Install on dev machine

```sh
# clone it
git clone https://bitbucket.org/nnkothari/test-project-web.git
cd test-project-web

# Install dependencies
npm install

# Start server
npm start

# web project available on
http://localhost:8080

# Build your web app
npm run build

```

Records available in following customer ID
- 123

