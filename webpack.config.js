const path = require('path');

const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

// Note:
// Instead of relying on the default "env" parameter passed
// via `webpack --env=production`, which does not allow other
// configuration values, We use cross-env so we can pass in
// any config we want as an object to node's process.env
module.exports = () => {
  const paths = {
    src: path.resolve(__dirname),
    dist: path.resolve(__dirname, 'dist'),
  };

  // Set environment variables as global to the react app
  const NODE_ENV = process.env.NODE_ENV || 'development';
  const VERSION = process.env.VERSION || 'unknown-version';

  // Helps injects code into the template HTML
  // to link the generated bundle.js.
  const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './src/index.html',
    favicon: './assets/favicon.ico',
    filename: 'index.html',
    inject: false,
    hash: true,

    // Note: Custom values can be injected into the template.
    // Refer: https://github.com/jantimon/html-webpack-plugin
    // At section: "Writing Your Own Templates" for example.
    // Example usage (not shown) is injecting google maps API key.
  });

  // Refer: https://webpack.js.org/plugins/define-plugin/
  // Expose environment-specific global variables app
  const envPlugin = {
    'process.env': {
      NODE_ENV: JSON.stringify(NODE_ENV), // e.g. "production". Used for string-based configs.
      [NODE_ENV.toUpperCase()]: true, // e.g. PRODUCTION: true. Used for bool-based configs.
      VERSION: JSON.stringify(VERSION), // e.g. "2.1.0"
    },
  };

  const DefinePlugin = new webpack.DefinePlugin(envPlugin);

  /* eslint-disable-next-line */
  console.log(envPlugin);

  // Clears older unused hashed files
  const CleanPlugin = new CleanWebpackPlugin(paths.dist, {
    verbose: true,
  });

  // Refer: https://webpack.js.org/plugins/module-concatenation-plugin/
  // Concatenates JS code into one closure, for faster code access.
  // Also, helps decreases filesize.
  const ConcatenationPlugin = new webpack.optimize.ModuleConcatenationPlugin();

  // Note:
  // Did not use UglifyJSPlugin for now. The gain is insignificant,
  // i.e. file size was only reduced from 760kb to 750kb.

  const config = {
    mode: NODE_ENV,

    entry: './index.js',

    context: paths.src, // base directory for resolving entry points and loaders

    output: {
      path: paths.dist,
      filename: 'bundle.js',
      publicPath: '/',
    },

    module: {
      rules: [
        {
          test: /\.(gif|png|ico|jpe?g)$/,
          use: [
            {
              loader: 'file-loader',
            },
            {
              loader: 'img-loader',
              options: {
                name: '[name]_[hash:5].[ext]',
              },
            },
          ],
        },

        {
          test: /\.svg$/,
          use: [
            'babel-loader',
            {
              loader: 'react-svg-loader',
              options: {
                svgo: {
                  plugins: [{ removeTitle: false }],
                  floatPrecision: 2,
                },
              },
            },
          ],
        },

        {
          test: /\.css$/,
          loader: 'style-loader!css-loader',
          include: /flexboxgrid/,
        },

        {
          // Refer: https://github.com/css-modules/css-modules/pull/65
          // Refer: .eslintrc.js file ("import/no-unresolved" rule)
          test: /\.css$/,
          exclude: /flexboxgrid/,
          oneOf: [
            {
              // Loads CSS without any special processing. Reserved
              // for third-party CSS (i.e. from node_modules)
              resourceQuery: /^\?raw$/,
              use: ['style-loader', 'css-loader'],
            },
            {
              // Loads module CSS, but without localIdentName to
              // prevent CSS pollution. Used for local CSS files to
              // override global third-party CSS, e.g. react-tippy
              resourceQuery: /^\?raw-module$/,
              use: [
                { loader: 'style-loader' },
                {
                  loader: require.resolve('css-loader'),
                  options: {
                    importLoaders: 1,
                    minimize: true,
                    sourceMap: true,
                    modules: true,
                    localIdentName: '[local]',
                  },
                },
                {
                  loader: 'postcss-loader',
                  options: {
                    plugins: () => [autoprefixer],
                  },
                },
              ],
            },
            {
              // Loads each CSS as module with unique localIdentName,
              // which prevents CSS pollution between app components.
              use: [
                { loader: 'style-loader' },
                {
                  loader: require.resolve('css-loader'),
                  options: {
                    importLoaders: 1,
                    minimize: true,
                    sourceMap: true,
                    modules: true,
                    localIdentName: '[name]__[local]__[hash:base64:5]',
                  },
                },
                {
                  // Refer:
                  // https://github.com/postcss/postcss-loader
                  // https://github.com/postcss/autoprefixer
                  //
                  // Notes:
                  // - 'postcss-loader' uses 'autoprefixer' as a plugin
                  // - May want to consider further configuring autoprefixer
                  loader: 'postcss-loader',
                  options: {
                    plugins: () => [autoprefixer],
                  },
                },
              ],
            },
          ],
        },

        {
          test: /\.(jsx|js)?$/,
          exclude: /node_modules/,
          use: [
            {
              // Notes:
              // - JSX and JS code are required to parse through babel-loader transpiler.
              // - 'env' preset refers to 'babel-preset-env'
              // - Order is important; eslint is performed last after code is transpiled.
              loader: 'babel-loader',
              options: {
                presets: ['@babel/react', '@babel/env'],
              },
            },
          ],
        },

        // This is needed for using the react-times library
        // for reference: https://github.com/ecmadao/react-times#config
        {
          test: /\.json$/,
          loader: 'json-loader',
          include: /json-loader/,
        },
      ],
    },

    resolve: {
      extensions: ['.json', '.js', '.jsx'],
    },

    devServer: {
      // Fixes issue with webpack-dev-server not able to load Push-State
      // URL (BrowserRouter) routes when reloading the page. Note, this
      // issue doesn't happen in production.
      historyApiFallback: true,
    },

    plugins: [DefinePlugin, HtmlWebpackPluginConfig, CleanPlugin, ConcatenationPlugin],
  };

  // Notes on which devtool mode to use:
  // // https://webpack.js.org/configuration/devtool/
  switch (NODE_ENV) {
    case 'production':
      // - Don't enable devtool in live code
      // - Best build and rebuild speed
      // - Bundled (minified) code

      config.output.filename = 'bundle.js';
      break;

    case 'development':
    default:
      config.devtool = 'source-map';
      config.output.filename = 'bundle.js';
      break;
  }

  return config;
};
