import { all } from 'redux-saga/effects';

import headerSaga from '../containers/header/header.saga';
import ordersSaga from '../containers/center-panel/centerPanel.saga';
import itemsSaga from '../containers/right-panel/rightPanel.saga';

function* rootSage() {
  yield all([headerSaga(), ordersSaga(), itemsSaga()]);
}

export default rootSage;
