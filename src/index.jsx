import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';

import store from './store';
import App from './containers/app/App';
import icons from './components/icons/icons';

import './index.css?raw';

// Setup FontAwesome icon library so we can use it anywhere in the app.
icons.setup();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
