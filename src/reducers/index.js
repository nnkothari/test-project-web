import { combineReducers } from 'redux';

import customerReducer from '../containers/header/header.reducer';
import OrdersReducer from '../containers/center-panel/centerPanel.reducer';
import ItemsReducer from '../containers/right-panel/rightPanel.reducer';

export default combineReducers({
  customer: customerReducer,
  orders: OrdersReducer,
  items: ItemsReducer,
});
