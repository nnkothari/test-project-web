import React from 'react';

const LeftPanel = props => (
  <nav className="sidebar">
    <ul className="nav flex-column pt-4">
      <li className="nav-item text-center">
        <a className="nav-link active" href="javascript:void(0)">
          Order
        </a>
      </li>
    </ul>
  </nav>
);

export default LeftPanel;
