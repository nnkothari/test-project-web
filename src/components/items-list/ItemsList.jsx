import React, { PureComponent } from 'react';
import FA from '@fortawesome/react-fontawesome';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';

import AddItemElement from '../add-item-element/AddItemElement';
import InfiniteScroll from '../../components/infinite-scroll/InfiniteScroll';


class ItemsList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { search: '' };
  }

  handleOnFormSubmit = e => {
    e.preventDefault();
    this.props.onSearch(this.state.search);
  };

  handleOnSearchChange = e => {
    this.setState({
      search: e.target.value,
    });
  };

  render() {
    return (<>
      <div className="right-bar border-left border-secondary bg-white pt-1">
        <div className="m-3">
          <div className="row">
            <div className="col-8">
              <button onClick={this.props.showOrdersList} className="back-button">
                <FA icon={['fas', 'chevron-left']} />
              </button>
              <h5 className="font-semibold">Details-Line Items</h5>
            </div>
            <div className="col-4 text-right">
              <button onClick={this.props.addNewItem} className="btn btn-primary btn-sm ">
                <FA icon={['fas', 'plus-circle']} /> Add
            </button>
            </div>
          </div>
        </div>
        <div>

          <Form
            className="form-group right-search"
            onSubmit={this.handleOnFormSubmit}
          >
            <InputGroup>
              <Form.Control
                type="text"
                value={this.state.search}
                onChange={this.handleOnSearchChange}
                placeholder="search..."
                aria-describedby="inputGroupPrepend"
              />
              <div className="input-group-prepend">
                <FA icon={['fas', 'search']} />
              </div>
            </InputGroup>
          </Form>
        </div>

        <div style={{ height: '430px', overflow: 'auto' }}>
          <InfiniteScroll onReachBottom={this.props.onBottom}>
            <table className="table table-borderless">
              <tbody>
                {
                  this.props.items.map((item, index) => (
                    <AddItemElement
                      itemIdToAdd={this.props.itemIdToAdd}
                      key={index}
                      item={item}
                      selectItemToAdd={this.props.selectItemToAdd}
                    />
                  ))
                }
              </tbody>
            </table>
          </InfiniteScroll>
        </div>
      </div >
    </>)
  }
}
export default ItemsList;
