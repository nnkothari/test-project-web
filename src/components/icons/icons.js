// Note: In case you want to import ALL icons for testing, uncomment below:
// import solid from '@fortawesome/fontawesome-free-solid';
// import brands from '@fortawesome/fontawesome-free-brands';

import addressCard from '@fortawesome/fontawesome-free-solid/faAddressCard';
import bars from '@fortawesome/fontawesome-free-solid/faBars';
import batteryHalf from '@fortawesome/fontawesome-free-solid/faBatteryHalf';
import bullseye from '@fortawesome/fontawesome-free-solid/faBullseye';
import caretDown from '@fortawesome/fontawesome-free-solid/faCaretDown';
import caretLeft from '@fortawesome/fontawesome-free-solid/faCaretLeft';
import caretRight from '@fortawesome/fontawesome-free-solid/faCaretRight';
import caretUp from '@fortawesome/fontawesome-free-solid/faCaretUp';
import ccvisa from '@fortawesome/fontawesome-free-brands/faCcVisa';
import chartPie from '@fortawesome/fontawesome-free-solid/faChartPie';
import check from '@fortawesome/fontawesome-free-solid/faCheck';
import checkCircle from '@fortawesome/fontawesome-free-solid/faCheckCircle';
import chevronDown from '@fortawesome/fontawesome-free-solid/faChevronDown';
import chevronLeft from '@fortawesome/fontawesome-free-solid/faChevronLeft';
import chevronUp from '@fortawesome/fontawesome-free-solid/faChevronUp';
import codeBranch from '@fortawesome/fontawesome-free-solid/faCodeBranch';
import cog from '@fortawesome/fontawesome-free-solid/faCog';
import cogs from '@fortawesome/fontawesome-free-solid/faCogs';
import comments from '@fortawesome/fontawesome-free-solid/faComments';
import copy from '@fortawesome/fontawesome-free-solid/faCopy';
import dotCircle from '@fortawesome/fontawesome-free-regular/faDotCircle';
import editRegular from '@fortawesome/fontawesome-free-regular/faEdit';
import ellipsisH from '@fortawesome/fontawesome-free-solid/faEllipsisH';
import exchangeAlt from '@fortawesome/fontawesome-free-solid/faExchangeAlt';
import exclamationCircle from '@fortawesome/fontawesome-free-solid/faExclamationCircle';
import fontawesome from '@fortawesome/fontawesome';
import infoCircle from '@fortawesome/fontawesome-free-solid/faInfoCircle';
import key from '@fortawesome/fontawesome-free-solid/faKey';
import mapMarkerAlt from '@fortawesome/fontawesome-free-solid/faMapMarkerAlt';
import pauseCircleRegular from '@fortawesome/fontawesome-free-regular/faPauseCircle';
import pauseCircleSolid from '@fortawesome/fontawesome-free-solid/faPauseCircle';
import pencilAlt from '@fortawesome/fontawesome-free-solid/faPencilAlt';
import playCircle from '@fortawesome/fontawesome-free-solid/faPlayCircle';
import questionCircle from '@fortawesome/fontawesome-free-solid/faQuestionCircle';
import search from '@fortawesome/fontawesome-free-solid/faSearch';
import share from '@fortawesome/fontawesome-free-solid/faShare';
import signOutAlt from '@fortawesome/fontawesome-free-solid/faSignOutAlt';
import sitemap from '@fortawesome/fontawesome-free-solid/faSitemap';
import stopCircle from '@fortawesome/fontawesome-free-solid/faStopCircle';
import store from '@fortawesome/fontawesome-free-solid/faStore';
import tags from '@fortawesome/fontawesome-free-solid/faTags';
import thumbsUp from '@fortawesome/fontawesome-free-solid/faThumbsUp';
import times from '@fortawesome/fontawesome-free-solid/faTimes';
import timesCircle from '@fortawesome/fontawesome-free-solid/faTimesCircle';
import trashAlt from '@fortawesome/fontawesome-free-solid/faTrashAlt';
import userCircle from '@fortawesome/fontawesome-free-solid/faUserCircle';
import users from '@fortawesome/fontawesome-free-solid/faUsers';
import plus from '@fortawesome/fontawesome-free-solid/faPlus';
import plusCircle from '@fortawesome/fontawesome-free-solid/faPlusCircle';

// Reference:
// https://github.com/FortAwesome/react-fontawesome#build-a-library-to-reference-icons-throughout-your-app-more-conveniently
// Note:
// If we import the whole icon pack (e.g. @fortawesome/fontawesome-free-brands),
// we don't have to call .add(...) below.

export const iconList = [
  addressCard,
  bars,
  batteryHalf,
  bullseye,
  caretDown,
  caretLeft,
  caretRight,
  caretUp,
  ccvisa,
  chartPie,
  check,
  checkCircle,
  chevronDown,
  chevronLeft,
  chevronUp,
  codeBranch,
  cog,
  cogs,
  comments,
  copy,
  dotCircle,
  editRegular,
  ellipsisH,
  exchangeAlt,
  exclamationCircle,
  infoCircle,
  key,
  mapMarkerAlt,
  pauseCircleRegular,
  pauseCircleSolid,
  pencilAlt,
  playCircle,
  questionCircle,
  search,
  share,
  signOutAlt,
  sitemap,
  stopCircle,
  store,
  tags,
  thumbsUp,
  times,
  timesCircle,
  trashAlt,
  userCircle,
  users,
  plus,
  plusCircle,
  // ALL icons, for testing purpose only
  // solid,
  // brands,
];

export default {
  setup: () => {
    fontawesome.library.add(...iconList);
  },
};
