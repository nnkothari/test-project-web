import React from 'react';
import PropTypes from 'prop-types';

const AddItemElement = props => (
  <tr>
    <th scope="row">
      <input
        type="radio"
        name="gender"
        value=""
        selected={props.itemIdToAdd === props._id}
        onChange={() => props.selectItemToAdd(props.item)}
      />
    </th>
    <td width="100px">
      <span className="text-secondary">{props.item.name}</span>
    </td>
    <td>
      <span className="text-secondary">Descripation</span>
      <p>{props.item.description}</p>
    </td>
  </tr>
);

AddItemElement.defaultProps = {
  item: {
    name: '',
    description: '',
  },
  selectItemToAdd: () => {},
};

AddItemElement.propTypes = {
  item: PropTypes.shape({ name: PropTypes.string, description: PropTypes.string }),
  selectItemToAdd: PropTypes.func.isRequired,
};

export default AddItemElement;
