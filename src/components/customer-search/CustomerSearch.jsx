import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import FA from '@fortawesome/react-fontawesome';

import css from './customerSearch.css';

class CustomerSearch extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { search: '' };
  }

  handleOnFormSubmit = e => {
    e.preventDefault();
    this.props.onSearch(this.state.search);
  };

  handleOnSearchChange = e => {
    this.setState({
      search: e.target.value,
    });
  };

  render() {
    return (
      <Form
        className="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0"
        onSubmit={this.handleOnFormSubmit}
      >
        <Form.Group>
          <InputGroup>
            <Form.Control
              type="text"
              value={this.state.search}
              onChange={this.handleOnSearchChange}
              placeholder="search..."
              aria-describedby="inputGroupPrepend"
            />
            <InputGroup.Prepend onClick={this.handleOnFormSubmit}>
              <InputGroup.Text
                id="inputGroupPrepend"
                className={[css.search__icon, ' btn btn-primary']}
              >
                <FA icon={['fas', 'search']} />
              </InputGroup.Text>
            </InputGroup.Prepend>
          </InputGroup>
        </Form.Group>
      </Form>
    );
  }
}

CustomerSearch.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

export default CustomerSearch;
