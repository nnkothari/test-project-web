import React from 'react';
import PropTypes from 'prop-types';

const CustomerTitle = props => (
  <div className="pt-2">
    Customer Lookup
    <h2>Client Name {props.customerId && props.customerName ? `-${props.customerName}` : null}</h2>
  </div>
);

CustomerTitle.defaultProps = {
  customerId: '',
  customerName: '',
};

CustomerTitle.propTypes = {
  customerName: PropTypes.string,
  customerId: PropTypes.string,
};

export default CustomerTitle;
