import React from 'react';
import FA from '@fortawesome/react-fontawesome';

import OrderItemDetail from '../order-item-detail/OrderItemDetail';

const OrderItemsList = props => (
  <div className="right-bar border-left border-secondary bg-white pt-1 order-item-container">
    <div>
      <div className="m-3">
        <div className="row">
          <div className="col-8">
            <h4 className="font-semibold">Details-Line Items</h4>
          </div>
          <div className="col-4 text-right">
            <button onClick={props.addItems} className="btn btn-primary btn-sm ">
              <FA icon={['fas', 'plus-circle']} /> Add
            </button>
          </div>
        </div>
      </div>

      {props.orderItems.map((item, index) => (
        <OrderItemDetail
          item={item}
          key={index}
          onDeleteClick={id => props.onDeleteClick(id)}
          updateItem={props.updateItem}
        />
      ))}
    </div>
  </div>
);

export default OrderItemsList;
