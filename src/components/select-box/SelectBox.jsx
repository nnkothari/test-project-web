import React from 'react';
import PropTypes from 'prop-types';
import Dropdown from 'react-bootstrap/Dropdown';

const SelectBox = props => (
  <Dropdown>
    <Dropdown.Toggle className="border-0 text-secondary p-2 col-4 text-left custom-dd" variant="secondary" id="dropdown-basic">
      {props.value}
    </Dropdown.Toggle>

    <Dropdown.Menu>
      <Dropdown.Item onSelect={() => props.onOrderChange('date')}>Most Recent</Dropdown.Item>
      <Dropdown.Item onSelect={() => props.onOrderChange('orderNo')}>Order Id</Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);

SelectBox.defauleProps = {
  onOrderChange: () => {},
};

SelectBox.propTypes = {
  onOrderChange: PropTypes.func.isRequired,
};

export default SelectBox;
