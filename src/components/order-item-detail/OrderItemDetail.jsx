import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import FA from '@fortawesome/react-fontawesome';

class OrderItemDetail extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isEditing: false,
      units: this.props.item.units,
    };
  }

  handleOnEditClick = () => {
    if (this.state.isEditing) {
      this.props.updateItem(this.props.item._id, this.props.item.details._id, this.state.units);
      this.setState({ isEditing: false });
    } else {
      this.setState({ ...this.state, isEditing: true });
    }
  };

  handleOnUnitChange = e => {
    this.setState({ ...this.state, units: e.target.value });
  };

  handleOnCancelRemoveClick = () => {
    if (this.state.isEditing) {
      this.setState({ ...this.state, units: this.props.item.units, isEditing: false });
    } else {
      this.props.onDeleteClick(this.props.item._id);
    }
  };

  render() {
    return (
      <div>
        <div className="item-info-box m-3">
          <div className="row">
            <div className="col-8 bg-light border-left border-secondary">
              <p className="pt-2 pb-2 mb-0 font-semibold">{this.props.item.details.name}</p>
            </div>
            <div className="col-4 text-right bg-light pr-0">
              <button
                onClick={this.handleOnCancelRemoveClick}
                className="btn btn-secondary btn-md rounded-0"
              >
                {this.state.isEditing ? (
                  <FA icon={['fas', 'times']} />
                ) : (
                  <FA icon={['fas', 'trash-alt']} />
                )}
              </button>
              <button
                onClick={this.handleOnEditClick}
                className="ml-1 btn btn-primary btn-md rounded-0"
              >
                {this.state.isEditing ? (
                  <FA icon={['fas', 'check']} />
                ) : (
                  <FA icon={['far', 'edit']} />
                )}
              </button>
            </div>
          </div>
        </div>
        <div className="item-info-box m-3">
          <div className="row">
            <div className="col-12 mb-3">
              <p className="mb-0 text-uppercase text-secondary">UOM</p>
              <p className="mb-0">{this.props.item.details.UOM}</p>
            </div>
            <div className="col-12 mb-3">
              <p className="mb-0 text-uppercase text-secondary">cost</p>
              <p className="mb-0">{this.props.item.details.cost}</p>
            </div>
            <div className="col-12 mb-3">
              <p className="mb-0 text-capitalize text-secondary">units</p>
              {this.state.isEditing ? (
                <input
                  className="form-group"
                  value={this.state.units}
                  onChange={this.handleOnUnitChange}
                />
              ) : (
                <p className="mb-0">{this.props.item.units}</p>
              )}
            </div>
            <div className="col-12 mb-0">
              <p className="mb-1 text-capitalize text-secondary">Description</p>
              <p className="mb-0">{this.props.item.details.description}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default OrderItemDetail;
