import React from 'react';

import styles from './Loader.css';

const Loader = props => (
  <div {...props}>
    <div className={styles.loaded} data-aid="loader">
      <div className={styles.loaders}>
        <div className={styles.loader}>
          <div className={styles.ball}>
            <div />
            <div />
            <div />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Loader;
