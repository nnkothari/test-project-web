import 'react-toastify/dist/ReactToastify.min.css?raw';
import { toast } from 'react-toastify';
import FA from '@fortawesome/react-fontawesome';
import React from 'react';

import styles from './toaster.css';
import stylesFail from './toaster-fail.css';
import stylesInfo from './toaster-info.css';
import stylesSuccess from './toaster-success.css';

export const baseConfig = {
  position: toast.POSITION.TOP_CENTER,
};

export const infoConfig = {
  ...baseConfig,
  className: stylesInfo.container,
  bodyClassName: stylesInfo.body,
  progressClassName: stylesInfo.progress,
};

export const successConfig = {
  ...baseConfig,
  className: stylesSuccess.container,
  bodyClassName: stylesSuccess.body,
  progressClassName: stylesSuccess.progress,
};

export const failConfig = {
  ...baseConfig,
  className: stylesFail.container,
  bodyClassName: stylesFail.body,
  progressClassName: stylesFail.progress,
};

export const noCloseConfig = {
  autoClose: false,
};

export const fadeConfig = {
  autoClose: 1800,
  draggable: true,
  closeOnClick: true,
  pauseOnHover: false,
};

export function createMessageWithIcon(dataAid, faIcon, message) {
  return (
    <div data-aid={dataAid} className={styles.content__body}>
      {!!faIcon && (
        <div className={styles.content__icon}>
          <FA icon={['fas', faIcon]} />
        </div>
      )}

      <span data-aid="toaster-message">{message}</span>
    </div>
  );
}

export function openToasterInfo(message) {
  return toast(createMessageWithIcon('toaster-info', '', message), {
    ...noCloseConfig,
    ...infoConfig,
  });
}

export function closeToasterSuccess(toastId, message) {
  return toast.update(toastId, {
    ...successConfig,
    ...fadeConfig,
    render: createMessageWithIcon('toaster-success', 'thumbs-up', message),
  });
}

export function closeToasterFail(toastId, message) {
  return toast.update(toastId, {
    ...failConfig,
    ...fadeConfig,
    render: createMessageWithIcon('toaster-fail', 'exclamation-circle', message),
  });
}

export default toast;
