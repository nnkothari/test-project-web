import React from 'react';
import PropTypes from 'prop-types';

const OrderRow = props => (
  <tr onClick={() => props.onRowClicked(props.order.orderNo)}>
    <td>{props.order.orderNo}</td>
    <td>{props.order.date}</td>
    <td>{props.order.zip}</td>
    <td>{props.order.state}</td>
    <td>{props.order.createdBy}</td>
    <td>{props.order.picked ? 'Yes' : 'No'}</td>
    <td>{props.order.shipped ? 'Yes' : 'No'}</td>
  </tr>
);

OrderRow.propTypes = {
  order: PropTypes.shape({
    orderNo: PropTypes.number,
    date: PropTypes.string,
    zip: PropTypes.string,
    state: PropTypes.string,
    createdBy: PropTypes.string,
    picked: PropTypes.bool,
    shipped: PropTypes.bool,
  }).isRequired,
  onRowClicked: PropTypes.func.isRequired,
};

export default OrderRow;
