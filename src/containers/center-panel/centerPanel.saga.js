import axios from 'axios';

import { takeEvery, put, all, select } from 'redux-saga/effects';

import {
  FETCH_ORDERS_BY_CUSTOMER_ID_REQUEST,
  FETCH_ORDERS_BY_CUSTOMER_ID_SUCCESS,
  FETCH_ORDERS_BY_CUSTOMER_ID_FAILURE,
  FETCH_CUSTOMER_BY_ID_SUCCESS,
  CHANGE_ORDERS_SORT_TYPE,
} from '../../constants/actions';
import { getCustomerId } from '../header/header.reducer';
import { openToasterInfo, closeToasterFail } from '../../components/toaster/toaster';
import apiEndPoint from '../../constants';

function* fetchOrdersByCustomerIdWorker({ page = 0, orderBy = 'date' }) {
  try {
    const customerId = yield select(getCustomerId);
    const orderType = orderBy === 'date' ? 'desc' : 'asc';
    const { data } = yield axios.get(
      `${apiEndPoint}getOrders?pageNumber=${page}&customerId=${customerId}&orderBy=${orderBy}&orderType=${orderType}`
    );
    const { totalRecord, orders } = data.data;
    const payLoad = { orders, totalRecord };

    yield put({
      type: FETCH_ORDERS_BY_CUSTOMER_ID_SUCCESS,
      payLoad,
    });
  } catch (error) {
    const toasterId = openToasterInfo('');
    closeToasterFail(toasterId, error || 'internal server error');
    put({ type: FETCH_ORDERS_BY_CUSTOMER_ID_FAILURE, error });
  }
}

function* ordersSage() {
  yield all([
    takeEvery(FETCH_ORDERS_BY_CUSTOMER_ID_REQUEST, fetchOrdersByCustomerIdWorker),
    takeEvery(FETCH_CUSTOMER_BY_ID_SUCCESS, fetchOrdersByCustomerIdWorker),
    takeEvery(CHANGE_ORDERS_SORT_TYPE, fetchOrdersByCustomerIdWorker),
  ]);
}

export default ordersSage;
