import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Row, Col, Table } from 'react-bootstrap';

import OrderRow from '../../components/order-row/OrderRow';
import SelectBox from '../../components/select-box/SelectBox';
import InfiniteScroll from '../../components/infinite-scroll/InfiniteScroll';

import css from './centerPanelContainer.css';
import { getCurrentPage, getOrders, getTotalRecords, getLoadingState } from './centerPanel.reducer';
import { getOrdersByCustomerId, selectOrder, changeOrderSort } from './centerPanel.actions';

class CenterPanelContainer extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      orderBy: 'Most Recent',
      hasMoreItems: true,
    };
  }

  onRowClicked(orderId) {
    this.props.selectOrder(orderId);
  }

  onBottom() {
    this.loadItems();
  }

  loadItems() {
    const page = parseInt(this.props.orders.length / 10, 10);

    if (this.props.orders.length < this.props.totalOrders) {
      this.props.getOrders(page, this.state.orderBy === 'Most Recent' ? 'date' : 'orderNo');
    }
  }

  handleOnOrderChanged(orderBy) {
    this.setState({ ...this.state, orderBy: orderBy === 'date' ? 'Most Recent' : 'orderNo' });
    this.props.changeOrderSort(0, orderBy === 'date' ? 'date' : 'orderNo');
  }

  render() {
    const rows = [];

    this.props.orders.forEach(order => {
      rows.push(
        <OrderRow
          key={order.orderNo}
          order={order}
          onRowClicked={orderId => this.onRowClicked(orderId)}
        />
      );
    });

    return (
      <>
        <Row className="m-b-30">
          <Col md={4}>
            Sort By
            <SelectBox
              onOrderChange={orderBy => this.handleOnOrderChanged(orderBy)}
              value={this.state.orderBy}
            />
          </Col>
        </Row>
        <div className={css.gridContainer}>
          <Table className={css.headerTable} responsive>
            <thead>
              <tr>
                <th>Order No</th>
                <th>Date</th>
                <th>Zip</th>
                <th>St</th>
                <th>Created By</th>
                <th>Picked</th>
                <th>Shipped</th>
              </tr>
            </thead>
          </Table>
          <div style={{ height: '430px', overflow: 'auto' }}>
            <InfiniteScroll onReachBottom={this.onBottom.bind(this)}>
              <Table className="table-striped" responsive>
                <tbody>{rows}</tbody>
              </Table>
            </InfiniteScroll>
          </div>
        </div>
      </>
    );
  }
}

CenterPanelContainer.defaultProps = {
  totalOrders: 0,
};

CenterPanelContainer.propTypes = {
  page: PropTypes.number.isRequired,
  orders: PropTypes.array.isRequired,
  totalOrders: PropTypes.number,
  isLoading: PropTypes.bool.isRequired,
  selectItems: PropTypes.func,
  getOrders: PropTypes.func.isRequired,
  selectOrder: PropTypes.func.isRequired,
  changeOrderSort: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  page: getCurrentPage(state),
  orders: getOrders(state),
  totalOrders: getTotalRecords(state),
  isLoading: getLoadingState(state),
});

const mapDispatchToProps = dispatch => ({
  getOrders: bindActionCreators(getOrdersByCustomerId, dispatch),
  selectOrder: bindActionCreators(selectOrder, dispatch),
  changeOrderSort: bindActionCreators(changeOrderSort, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CenterPanelContainer);
