import {
  SELECT_ORDER,
  FETCH_ITEMS_BY_ORDER_ID_SUCCESS,
  FETCH_ITEMS_BY_ORDER_ID_FAILURE,
  FETCH_ORDERS_BY_CUSTOMER_ID_REQUEST,
  FETCH_ORDERS_BY_CUSTOMER_ID_SUCCESS,
  FETCH_ORDERS_BY_CUSTOMER_ID_FAILURE,
  FETCH_CUSTOMER_BY_ID_SUCCESS,
  FETCH_CUSTOMER_BY_ID_FAILURE,
  CHANGE_ORDERS_SORT_TYPE,
} from '../../constants/actions';

const initialState = {
  orders: [],
  selectedOrder: null,
  page: 0,
  totalOrders: 0,
  isLoadingOrders: true,
  isLoading: false,
  error: null,
};

const parseDate = date => {
  try {
    const newDate = new Date(date);
    return `${newDate.getMonth()} - ${newDate.getDate()} - ${newDate.getFullYear()}`;
  } catch (error) {
    return '';
  }
};

const parseOrdersData = data => {
  const records = data.map(order => ({
    orderNo: order.orderNo,
    date: order.date ? parseDate(order.date) : '',
    zip: order.zip || '',
    state: order.state || '',
    createdBy: order.createdBy || '',
    picked: order.picked,
    shipped: order.shipped,
  }));
  return records;
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CUSTOMER_BY_ID_SUCCESS:
      return { ...state, orders: [], totalOrders: 0 };
    case FETCH_ORDERS_BY_CUSTOMER_ID_REQUEST:
      return { ...state, isLoadingOrders: true, isLoading: true };

    case FETCH_ORDERS_BY_CUSTOMER_ID_SUCCESS:
      return {
        ...state,
        isLoadingOrders: false,
        isLoading: false,
        orders: [...state.orders, ...parseOrdersData(action.payLoad.orders)],
        totalOrders: action.payLoad.totalRecord,
      };

    case FETCH_CUSTOMER_BY_ID_FAILURE:
      return {
        ...state,
        isLoadingOrders: false,
        isLoading: false,
        orders: [],
        totalOrders: 0,
      };

    case FETCH_ORDERS_BY_CUSTOMER_ID_FAILURE:
      return { ...state, isLoadingOrders: false, orders: [] };

    case SELECT_ORDER:
      return { ...state, isLoadingOrders: true, selectedOrder: action.orderId };

    case FETCH_ITEMS_BY_ORDER_ID_SUCCESS:
      return { ...state, isLoadingOrders: false, items: action.items };

    case FETCH_ITEMS_BY_ORDER_ID_FAILURE:
      return { ...state, isLoadingOrders: false, error: action.error };
    case CHANGE_ORDERS_SORT_TYPE:
      return { ...state, orders: [] };

    default:
      return state;
  }
};

// selectors

export const getCurrentPage = state => state.orders.page;
export const getTotalRecords = state => state.orders.totalOrders;
export const getOrders = state => state.orders.orders;

export const getLoadingState = state => state.orders.isLoading;

export const selectedOrderId = state => state.orders.selectedOrder;
