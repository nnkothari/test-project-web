import {
  SELECT_ORDER,
  FETCH_ORDERS_BY_CUSTOMER_ID_REQUEST,
  CHANGE_ORDERS_SORT_TYPE,
} from '../../constants/actions';

export const selectOrder = orderId => ({
  type: SELECT_ORDER,
  orderId,
});

export const getOrdersByCustomerId = (page, orderBy = 'date') => ({
  type: FETCH_ORDERS_BY_CUSTOMER_ID_REQUEST,
  ...{ page, orderBy },
});

export const changeOrderSort = (page, orderBy) => ({
  type: CHANGE_ORDERS_SORT_TYPE,
  ...{ page, orderBy },
});
