import axios from 'axios';

import { takeEvery, put, all, select } from 'redux-saga/effects';

import {
  SELECT_ORDER,
  FETCH_ITEMS_BY_ORDER_ID_SUCCESS,
  FETCH_ITEMS_BY_ORDER_ID_FAILURE,
  FETCH_ALL_ITEMS_SUCCESS,
  FETCH_ALL_ITEMS_FAILURE,
  FETCH_ALL_ITEMS_REQUEST,
  ADD_NEW_ITEM_REQUEST,
  ADD_NEW_ITEM_SUCCESS,
  ADD_NEW_ITEM_FAILURE,
  DELETE_ORDER_ITEM_REQUEST,
  DELETE_ORDER_ITEM_SUCCESS,
  DELETE_ORDER_ITEM__FAILURE,
  UPDATE_ORDER_ITEM_SUCCESS,
  UPDATE_ORDER_ITEM__FAILURE,
  UPDATE_ORDER_ITEM_REQUEST,
  CHANGE_ITEMS_SEARCH,
  SEARCH_ALL_ITEMS_SUCCESS
} from '../../constants/actions';
import { selectedOrderId } from '../center-panel/centerPanel.reducer';
import {
  openToasterInfo,
  closeToasterSuccess,
  closeToasterFail,
} from '../../components/toaster/toaster';

import { getItemToAddId } from './rightPanel.reducer';
import apiEndPoint from '../../constants';

function* fetchItemsByOrderIdWorker({ orderId }) {
  try {
    const { data } = yield axios.get(
      `${apiEndPoint}getItemByOrderId?orderNo=${orderId}`
    );

    yield put({
      type: FETCH_ITEMS_BY_ORDER_ID_SUCCESS,
      items: data.data.orders[0].items,
    });
  } catch (error) {
    put({ type: FETCH_ITEMS_BY_ORDER_ID_FAILURE, error });
  }
}

function* fetchAllItemsWorker({ page = 0, searchString = '' }) {
  try {
    const { data } = yield axios.get(
      `${apiEndPoint}getItems?pageNumber=${page}&searchString=${searchString}`
    );
    const { totalRecord, items } = data.data;
    const payLoad = { items, totalRecord };
    searchString ?
      yield put({
        type: SEARCH_ALL_ITEMS_SUCCESS,
        payLoad,
      })
      :
      yield put({
        type: FETCH_ALL_ITEMS_SUCCESS,
        payLoad,
      })
  } catch (error) {
    put({ type: FETCH_ALL_ITEMS_FAILURE, error });
  }
}

function* addItemWorker() {
  const toasterId = openToasterInfo('adding...');

  try {
    const orderNo = yield select(selectedOrderId);
    const itemId = yield select(getItemToAddId);

    if (orderNo && itemId) {
      const response = yield axios.post(`${apiEndPoint}addItems`, {
        itemId,
        orderNo,
      });
      closeToasterSuccess(toasterId, 'item added successfully');

      yield put({
        type: ADD_NEW_ITEM_SUCCESS,
        orderId: orderNo,
      });
    }
  } catch (error) {
    closeToasterSuccess(toasterId, 'error while adding item');
    put({ type: ADD_NEW_ITEM_FAILURE, error });
  }
}

function* deleteOrderItemWorker({ id }) {
  const toasterId = openToasterInfo('deleting...');

  try {
    const response = yield axios.delete(`${apiEndPoint}deleteItem?itemId=${id}`);

    const orderNo = yield select(selectedOrderId);
    closeToasterSuccess(toasterId, 'item deleted successfully');

    yield put({
      type: DELETE_ORDER_ITEM_SUCCESS,
      orderId: orderNo,
    });
  } catch (error) {
    closeToasterFail(toasterId, 'error while deleting item');
    put({ type: DELETE_ORDER_ITEM__FAILURE, error });
  }
}

function* updateItemWorker({ payLoad }) {
  const toasterId = openToasterInfo('updating...');

  try {
    const { id, units, itemsId } = payLoad;
    const response = yield axios.put(`${apiEndPoint}updateItem`, {
      itemId: id,
      itemDetailsId: itemsId,
      units,
    });
    const orderNo = yield select(selectedOrderId);
    closeToasterSuccess(toasterId, 'item updated successfully');

    yield put({
      type: UPDATE_ORDER_ITEM_SUCCESS,
      orderId: orderNo,
    });
  } catch (error) {
    closeToasterFail(toasterId, 'error while updating item');
    put({ type: UPDATE_ORDER_ITEM__FAILURE, error });
  }
}

function* itemsSaga() {
  yield all([
    takeEvery(SELECT_ORDER, fetchItemsByOrderIdWorker),
    takeEvery(FETCH_ALL_ITEMS_REQUEST, fetchAllItemsWorker),
    takeEvery(ADD_NEW_ITEM_REQUEST, addItemWorker),
    takeEvery(DELETE_ORDER_ITEM_REQUEST, deleteOrderItemWorker),
    takeEvery(DELETE_ORDER_ITEM_SUCCESS, fetchItemsByOrderIdWorker),
    takeEvery(ADD_NEW_ITEM_SUCCESS, fetchItemsByOrderIdWorker),
    takeEvery(UPDATE_ORDER_ITEM_REQUEST, updateItemWorker),
    takeEvery(UPDATE_ORDER_ITEM_SUCCESS, fetchItemsByOrderIdWorker),
    takeEvery(CHANGE_ITEMS_SEARCH, fetchAllItemsWorker),
  ]);
}

export default itemsSaga;
