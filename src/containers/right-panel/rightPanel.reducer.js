import {
  SELECT_ORDER,
  TOGGLE_RIGHT_PANEL,
  FETCH_ITEMS_BY_ORDER_ID_SUCCESS,
  FETCH_ALL_ITEMS_REQUEST,
  FETCH_ALL_ITEMS_SUCCESS,
  SELECT_ITEM_TO_ADD,
  ADD_NEW_ITEM_SUCCESS,
  SEARCH_ALL_ITEMS_SUCCESS,
  SHOW_ORDERS_ITEMS_LIST_CLICKED
} from '../../constants/actions';

const initialState = {
  showAddIemsList: false,
  showOrderItemsList: false,
  orderItems: [],
  items: [],
  itemId: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SELECT_ORDER:
      return { ...state, showOrderItemsList: true };

    case TOGGLE_RIGHT_PANEL:
      return { ...state, showOrderItemsList: action.showOrderItems };

    case FETCH_ITEMS_BY_ORDER_ID_SUCCESS:
      return { ...state, orderItems: [...action.items] };

    case FETCH_ALL_ITEMS_REQUEST:
      return { ...state, showAddIemsList: true, showOrderItemsList: false };

    case FETCH_ALL_ITEMS_SUCCESS:
      return { ...state, items: [...state.items, ...action.payLoad.items], totalItems: action.payLoad.totalRecord };

    case SEARCH_ALL_ITEMS_SUCCESS:
      return { ...state, items: [...action.payLoad.items], totalItems: action.payLoad.totalRecord };


    case SELECT_ITEM_TO_ADD:
      return { ...state, itemId: action.id };

    case ADD_NEW_ITEM_SUCCESS:
      return { ...state, showAddIemsList: false, showOrderItemsList: true };

    case SHOW_ORDERS_ITEMS_LIST_CLICKED:
      return { ...state, showAddIemsList: false, showOrderItemsList: true };

    default:
      return state;
  }
};

// -- selectors

export const isShowAddItemsList = state => state.items.showAddIemsList;
export const isShowOrderItemsList = state => state.items.showOrderItemsList;
export const getOrderItems = state => state.items.orderItems;
export const getItems = state => state.items.items;

export const getItemToAddId = state => state.items.itemId;
export const getTotalItems = state => state.items.totalItems;
