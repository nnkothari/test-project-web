import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FA from '@fortawesome/react-fontawesome';

import ItemsList from '../../components/items-list/ItemsList';
import OrderItemsDetails from '../../components/order-items-list/OrderItemsList';

import {
  changeSearchString,
  toggleRightPanel,
  getAllItems,
  selectItemToAdd,
  addNewItem,
  deleteItem,
  updateItem,
  showOrdersList,
} from './rightPanel.actions';
import css from './rightPanelContainer.css';
import {
  isShowOrderItemsList,
  isShowAddItemsList,
  getOrderItems,
  getItems,
  getItemToAddId,
  getTotalItems
} from './rightPanel.reducer';

// eslint-disable-next-line react/prefer-stateless-function
class RightPanelContainer extends PureComponent {

  onBottom = () => {
    this.loadItems();
  }

  loadItems() {
    const page = parseInt(this.props.addItems.length / 10, 10);

    if (this.props.addItems.length < this.props.totalItems) {
      this.props.addItemsClicked(page, '');
    }
  }

  onSearch = searchString => {
    this.props.searchItems(0, searchString);
  }

  render() {
    const { showOrderItems, isShowItemsList, orderItems, allItems, itemIdToAdd } = this.props;
    return (
      <div className={css.right__sidebar}>
        <div className="info">
          <button
            onClick={() => this.props.toggleRightPanel(!showOrderItems)}
            className="btn btn-primary btn-block"
          >
            <FA icon={['fas', 'info-circle']} />
          </button>
          {showOrderItems ? (
            <OrderItemsDetails
              orderItems={orderItems}
              addItems={() => this.props.addItemsClicked(0, '')}
              orderItems={orderItems}
              onDeleteClick={this.props.deleteItem}
              updateItem={this.props.updateItem}
            />
          ) : null}
          {isShowItemsList ? (
            <ItemsList
              showOrdersList={this.props.showOrdersList}
              onBottom={this.onBottom}
              onSearch={this.onSearch}
              items={allItems}
              itemIdToAdd={itemIdToAdd}
              selectItemToAdd={this.props.selectItemToAdd}
              addNewItem={this.props.addNewItem}
            />
          ) : null}
        </div>
      </div>
    );
  }
}

RightPanelContainer.defaultProps = {
  itemIdToAdd: '',
};

RightPanelContainer.propTypes = {
  showOrderItems: PropTypes.bool.isRequired,
  isShowItemsList: PropTypes.bool.isRequired,
  orderItems: PropTypes.array.isRequired,
  allItems: PropTypes.array.isRequired,
  selectItemToAdd: PropTypes.func.isRequired,
  itemIdToAdd: PropTypes.string,
  addItemsClicked: PropTypes.func.isRequired,
  deleteItem: PropTypes.func.isRequired,
  addNewItem: PropTypes.func.isRequired,
  toggleRightPanel: PropTypes.func.isRequired,
  updateItem: PropTypes.func.isRequired,
  showOrdersList: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  showOrderItems: isShowOrderItemsList(state),
  isShowItemsList: isShowAddItemsList(state),
  orderItems: getOrderItems(state),
  allItems: getItems(state),
  itemIdToAdd: getItemToAddId(state),
  addItems: getItems(state),
  totalItems: getTotalItems(state),
});

const mapDispatchToProps = dispatch => ({
  toggleRightPanel: bindActionCreators(toggleRightPanel, dispatch),
  addItemsClicked: bindActionCreators(getAllItems, dispatch),
  selectItemToAdd: bindActionCreators(selectItemToAdd, dispatch),
  addNewItem: bindActionCreators(addNewItem, dispatch),
  deleteItem: bindActionCreators(deleteItem, dispatch),
  updateItem: bindActionCreators(updateItem, dispatch),
  searchItems: bindActionCreators(changeSearchString, dispatch),
  showOrdersList: bindActionCreators(showOrdersList, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RightPanelContainer);
