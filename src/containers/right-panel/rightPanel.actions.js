import {
    TOGGLE_RIGHT_PANEL,
    FETCH_ALL_ITEMS_REQUEST,
    SELECT_ITEM_TO_ADD,
    ADD_NEW_ITEM_REQUEST,
    DELETE_ORDER_ITEM_REQUEST,
    UPDATE_ORDER_ITEM_REQUEST,
    CHANGE_ITEMS_SEARCH,
    SHOW_ORDERS_ITEMS_LIST_CLICKED
} from '../../constants/actions';


export const toggleRightPanel = showOrderItems => ({
    type: TOGGLE_RIGHT_PANEL,
    showOrderItems,
});


export const getAllItems = (page, searchString = '') => ({
    type: FETCH_ALL_ITEMS_REQUEST,
    ...{ page, searchString },
});

export const changeSearchString = (page, searchString) => ({
    type: CHANGE_ITEMS_SEARCH,
    ...{ page, searchString },
});

export const addNewItem = () => ({
    type: ADD_NEW_ITEM_REQUEST,
});

export const deleteItem = id => ({
    type: DELETE_ORDER_ITEM_REQUEST,
    id,
});

export const updateItem = (id, itemsId, units) => ({
    type: UPDATE_ORDER_ITEM_REQUEST,
    payLoad: { id, itemsId, units },
});

export const showOrdersList = () => ({
    type: SHOW_ORDERS_ITEMS_LIST_CLICKED,
});


export const selectItemToAdd = data => ({ type: SELECT_ITEM_TO_ADD, id: data._id });
