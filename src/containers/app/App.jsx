import React from 'react';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';

import RightPanel from '../right-panel/RightPanelContainer';
import CenterPanel from '../center-panel/CenterPanelContainer';
import Header from '../header/HeaderContainer';
import LeftPanel from '../../components/left-panel/LeftPanel';

const App = props => (
  <div>
    <ToastContainer newestOnTop draggable={false} closeButton={false} closeOnClick={false} />
    <Header />
    <div className="container-fluid page-body-wrapper">
      <LeftPanel />

      <div className="main-panel">
        <div className="content-wrapper">
          <CenterPanel />
        </div>
      </div>

      <RightPanel />
    </div>
  </div>
);

export default connect()(App);
