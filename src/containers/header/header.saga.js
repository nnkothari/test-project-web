import axios from 'axios';

import { takeEvery, put, all } from 'redux-saga/effects';

import apiEndPoint from '../../constants';

import {
  FETCH_CUSTOMER_BY_ID_REQUEST,
  FETCH_CUSTOMER_BY_ID_SUCCESS,
  FETCH_CUSTOMER_BY_ID_FAILURE,
} from '../../constants/actions';
import {
  openToasterInfo,
  closeToasterSuccess,
  closeToasterFail,
} from '../../components/toaster/toaster';

function* fetchCustomerByIdWorker({ id }) {
  try {
    const { data } = yield axios.get(`${apiEndPoint}getCustomerById?customerId=${id}`);

    if (data.data) {
      const { customerName, customerId } = data.data;

      yield put({
        type: FETCH_CUSTOMER_BY_ID_SUCCESS,
        customer: {
          customerName,
          customerId,
        },
      });
    } else {
      const toastId = openToasterInfo('');
      closeToasterFail(toastId, 'Customer not found!');
      yield put({ type: FETCH_CUSTOMER_BY_ID_FAILURE, error: 'Customer not found!' });
    }
  } catch (error) {
    put({ type: FETCH_CUSTOMER_BY_ID_FAILURE, error });
  }
}

function* headerSaga() {
  yield all([takeEvery(FETCH_CUSTOMER_BY_ID_REQUEST, fetchCustomerByIdWorker)]);
}

export default headerSaga;
