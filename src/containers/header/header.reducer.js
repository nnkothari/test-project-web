import {
  FETCH_CUSTOMER_BY_ID_SUCCESS,
  FETCH_CUSTOMER_BY_ID_FAILURE,
} from '../../constants/actions';

const initialState = {
  customerId: '',
  customerName: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CUSTOMER_BY_ID_SUCCESS:
      return {
        ...state,
        ...action.customer,
      };
    case FETCH_CUSTOMER_BY_ID_FAILURE:
      return {
        ...state,
        customerId: '',
        customerName: '',
      };

    default:
      return state;
  }
};

// selectors

export const getCustomerName = state => state.customer.customerName;
export const getCustomerId = state => state.customer.customerId;
