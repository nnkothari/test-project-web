import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Row, Col } from 'react-bootstrap';

import CustomerTitle from '../../components/customer-title/CustomerTitle';
import CustomerSearch from '../../components/customer-search/CustomerSearch';
import userIcon from '../../../assets/images/face1.jpg';
import logo from '../../../assets/images/logo-small.png';

import { getCustomerName, getCustomerId } from './header.reducer';
import { getCustomerById } from './header.actions';

class HeaderContainer extends PureComponent {
  handleOnSearch = value => {
    this.props.searchCustomerById(value);
  };

  render() {
    const { customerName, customerId } = this.props;

    return (
      <header className="main-header fixed-top">
        <nav className="navbar nevbar-main">
          <div className="text-center navbar-brand-wrapper">
            <a className="navbar-brand brand-logo">
              <img className="navbar-brand-logo" src={logo} alt="Logo" />
            </a>
          </div>
          <div className="navbar-menu-wrapper border-bottom">
            <Row>
              <Col className="d-flex align-items-center h-100" lg={8} md={8} sm={8} xs={6}>
                <CustomerTitle customerId={customerId} customerName={customerName} />
              </Col>
              <Col lg={4} md={4} sm={4} xs={6}>
                <div className="d-flex align-items-center h-100">
                  <CustomerSearch onSearch={this.handleOnSearch} />
                  <div className="header-login">
                    <img className="img-xs rounded-circle" src={userIcon} alt="user" />
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </nav>
      </header>
    );
  }
}

HeaderContainer.propTypes = {
  customerName: PropTypes.string.isRequired,
  customerId: PropTypes.string.isRequired,
  searchCustomerById: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  customerName: getCustomerName(state),
  customerId: getCustomerId(state),
});

const mapDispatchToProps = dispatch => ({
  searchCustomerById: bindActionCreators(getCustomerById, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderContainer);
