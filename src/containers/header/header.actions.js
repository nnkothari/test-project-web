import { FETCH_CUSTOMER_BY_ID_REQUEST } from '../../constants/actions';

export const getCustomerById = id => {
  return { type: FETCH_CUSTOMER_BY_ID_REQUEST, id };
};
