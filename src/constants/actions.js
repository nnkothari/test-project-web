export const FETCH_CUSTOMER_BY_ID_REQUEST = 'FETCH_CUSTOMER_REQUEST';
export const FETCH_CUSTOMER_BY_ID_SUCCESS = 'FETCH_CUSTOMER_BY_ID_SUCCESS';
export const FETCH_CUSTOMER_BY_ID_FAILURE = 'FETCH_CUSTOMER_BY_ID_FAILURE';

export const SELECT_ORDER = 'SELECT_ORDER';
export const FETCH_ITEMS_BY_ORDER_ID_SUCCESS = 'FETCH_ITEMS_BY_ORDER_ID_SUCCESS';
export const FETCH_ITEMS_BY_ORDER_ID_FAILURE = 'FETCH_ITEMS_BY_ORDER_ID_FAILURE';

export const FETCH_ORDERS_BY_CUSTOMER_ID_REQUEST = 'FETCH_ORDERS_BY_CUSTOMER_ID_REQUEST';
export const FETCH_ORDERS_BY_CUSTOMER_ID_SUCCESS = 'FETCH_ORDERS_BY_CUSTOMER_ID_SUCCESS';
export const FETCH_ORDERS_BY_CUSTOMER_ID_FAILURE = 'FETCH_ORDERS_BY_CUSTOMER_ID_FAILURE';

export const TOGGLE_RIGHT_PANEL = 'TOGGLE_RIGHT_PANEL';

export const FETCH_ALL_ITEMS_REQUEST = 'FETCH_ALL_ITEMS_REQUEST';
export const FETCH_ALL_ITEMS_SUCCESS = 'FETCH_ALL_ITEMS_SUCCESS';
export const FETCH_ALL_ITEMS_FAILURE = 'FETCH_ALL_ITEMS_FAILURE';

export const CHANGE_ORDERS_SORT_TYPE = 'CHANGE_ORDERS_SORT_TYPE';

export const SELECT_ITEM_TO_ADD = 'SELECT_ITEM_TO_ADD';

export const ADD_NEW_ITEM_REQUEST = 'ADD_NEW_ITEM_REQUEST';
export const ADD_NEW_ITEM_SUCCESS = 'ADD_NEW_ITEM_SUCCESS';
export const ADD_NEW_ITEM_FAILURE = 'ADD_NEW_ITEM_FAILURE';

export const DELETE_ORDER_ITEM_REQUEST = 'DELETE_ORDER_ITEM_REQUEST';
export const DELETE_ORDER_ITEM_SUCCESS = 'DELETE_ORDER_ITEM_SUCCESS';
export const DELETE_ORDER_ITEM__FAILURE = 'DELETE_ORDER_ITEM__FAILURE';

export const UPDATE_ORDER_ITEM_REQUEST = 'UPDATE_ORDER_ITEM_REQUEST';
export const UPDATE_ORDER_ITEM_SUCCESS = 'UPDATE_ORDER_ITEM_SUCCESS';
export const UPDATE_ORDER_ITEM__FAILURE = 'UPDATE_ORDER_ITEM__FAILURE';
export const CHANGE_ITEMS_SEARCH = 'CHANGE_ITEMS_SEARCH';

export const SEARCH_ALL_ITEMS_SUCCESS = 'SEARCH_ALL_ITEMS_SUCCESS';

export const SHOW_ORDERS_ITEMS_LIST_CLICKED = 'SHOW_ORDERS_ITEMS_LIST_CLICKED';
