
export const { NODE_ENV } = process.env;
let apiEndPoint;

if (NODE_ENV === 'development') {
    apiEndPoint = 'http://localhost:3000/api/';
} else {
    apiEndPoint = 'http://localhost:3000/api/';
}

export default apiEndPoint;